#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter.ttk import *
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
import json, sys

root = tk.Tk()
root.geometry("550x300+300+150")
root.resizable(0,0)
root.title("Editor")

filepath = sys.argv[1]

jsonfile = open(filepath, "r+")
data = json.load(jsonfile)
values = sorted(data.keys())


combo1 = ttk.Combobox(root)
combo2 = ttk.Combobox(root)
combo3 = ttk.Combobox(root)
combo4 = ttk.Combobox(root)
combo5 = ttk.Combobox(root)

entry1 =  ScrolledText(root)

var1 = tk.StringVar()
var2 = tk.StringVar()
var3 = tk.StringVar()
var4 = tk.StringVar()
var5 = tk.StringVar()

combo1.place(x=40, y=30, height=24, width=100)
combo1["values"]=values
combo1.configure(state='readonly', textvariable=var1)

combo2.place(x=40, y=70, height=24, width=100)

combo2.configure(state='readonly', textvariable=var2)

combo3.place(x=40, y=110, height=24, width=100)
combo3.configure(state='readonly', textvariable=var3)

combo4.place(x=40, y=150, height=24, width=100)
combo4.configure(state='readonly', textvariable=var4)

combo5.place(x=40, y=190, height=24, width=100)
combo5.configure(state='readonly', textvariable=var5)

entry1.place(x=200, y=20, height=220, width=300)



def defocus(event):
    combo1.selection_clear()
    combo2.set('')
    combo2["values"] = []
    combo3.set('')
    combo2["values"] = []
    combo4.set('')
    combo4["values"] = []
    combo5.set('')
    combo5["values"] = []
    bkey = combo1.get()
    if bkey:
        block = data[bkey]
        combo2["values"] = sorted(block.keys())

combo1.bind("<FocusIn>", defocus)

def defocus2(event):
    combo2.selection_clear()
    combo3.set('')
    combo3["values"] = []
    combo4.set('')
    combo4["values"] = []
    combo5.set('')
    combo5["values"] = []
    bkey = combo2.get()
    if bkey:
        wid = data[combo1.get()][bkey]
        if isinstance(wid,dict):
            combo3["values"] =  sorted(wid.keys())
        else:
            val = data[combo1.get()][bkey]
            entry1.delete(0.0, 'end')
            entry1.insert(tk.END, str(val))


combo2.bind("<FocusIn>", defocus2)

def defocus3(event):
    combo3.selection_clear()
    combo4.set('')
    combo4["values"] = []
    combo5.set('')
    combo5["values"] = []
    bkey = combo3.get()
    if bkey:
        val = data[combo1.get()][combo2.get()][bkey]
        if isinstance(val,dict):
            combo4["values"] =  sorted(val.keys())
        else:
            entry1.delete(0.0, 'end')
            entry1.insert(tk.END, str(val))

combo3.bind("<FocusIn>", defocus3)

def defocus4(event):
    combo4.selection_clear()
    combo5.set('')
    combo5["values"] = []
    bkey = combo4.get()
    if bkey:
        val = data[combo1.get()][combo2.get()][combo3.get()][bkey]
        if isinstance(val,dict):
            combo5["values"] =  sorted(val.keys())
        else:
            entry1.delete(0.0, 'end')
            entry1.insert(tk.END, str(val))

combo4.bind("<FocusIn>", defocus4)

def defocus5(event):
    combo5.selection_clear()
    bkey = combo5.get()
    if bkey:
        val = data[combo1.get()][combo2.get()][combo3.get()][combo4.get()][bkey]
        entry1.delete(0.0, 'end')
        entry1.insert(tk.END, str(val))

combo5.bind("<FocusIn>", defocus5)


def save():
    val = data
    wdict = data
    key = None
    keys = []
    keys.append(combo1.get())
    keys.append(combo2.get())
    keys.append(combo3.get())
    keys.append(combo4.get())
    keys.append(combo5.get())

    for k in keys:
        if k:
            wdict = val
            val = wdict[k]
            key = k

    nval = entry1.get("0.0",tk.END)
    nval = nval.rstrip()
    print(nval)
    if type(val) == int or type(val) == float:
        wdict[key] = float(nval)
    elif type(val) == list:
        wdict[key] = list(eval(nval))
    else:
        wdict[key] = nval

    jsonfile.seek(0)
    jsonstr = json.dumps(data, sort_keys=True, indent=2)
    jsonfile.write(jsonstr)
    jsonfile.truncate()

tkokButton = tk.Button(
    root,
    text="Ok",
    command=save)

tkokButton.place(relx=0.1, rely=0.9, relheight=.1, relwidth=.1)

def exit():
    jsonfile.close()
    root.destroy()

tkexitButton = tk.Button(
    root,
    text="Exit",
    command=exit)

tkexitButton.place(relx=0.9, rely=0.9, relheight=.1, relwidth=.1)



tk.mainloop()
