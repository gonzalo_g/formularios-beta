#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json, sys


def load(fpath):
    filepath = fpath
    jsonfile = open(filepath, "r+")
    data = json.load(jsonfile)
    return data, jsonfile

def save(data, jsonfile):
    jsonfile.seek(0)
    jsonstr = json.dumps(data, sort_keys=True, indent=2)
    jsonfile.write(jsonstr)
    jsonfile.truncate()
    jsonfile.close()

def show_keys(data, keys):
    if isinstance(data,dict):
        for k in sorted(data.keys()):
            print(k)
        key = input("keys:\n")
        keys.append(key)
        show_keys(data[key], keys)
    elif isinstance(data,list):
        print(data)
        index = input("index:\n")
        keys.append(index)
        print(data[int(index)])
        print(keys)
    else:
        print(data)
        print(keys)

def main():
    data, file = load(sys.argv[1])
    show_keys(data, [])

if __name__ == "__main__":
    # execute only if run as a script
    main()
