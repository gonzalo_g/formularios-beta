import tkinter as tk
from tkinter.ttk import *
from tkinter import ttk
import json
import os

class Form:

    def __init__(self, root, name):
        notebook = ttk.Notebook(root)
        notebook.place(x=0, y=0, height=540, width=815)
        directory = os.path.join('layaout', 'form'+name)
        self.blocks = []

        for i, file in enumerate(sorted(os.listdir(directory))):
            filename = os.fsdecode(file)
            filepath = os.path.join(directory, filename)
            frames = []
            if filename.endswith(".json"):
                with open(filepath) as json_data:
                    tab = ttk.Frame(notebook)
                    frames.append(tab)
                    data = json.load(json_data)
                    for key, jblock in sorted(data.items()):
                        self.blocks.append(Block(tab, jblock, frames))
                    notebook.add(tab, text='Pestaña {}'.format(i))
                continue

    def get_data(self):
        data = []
        for b in self.blocks:
            data.extend(b.get_data())
        return data


class Block:

    entries = []
    labels = []
    radios = []
    combos = []
    rv = []
    cv = []
    sv = []
    d = {}

    def handler(self, svar):
        print(svar.get())

    def __init__(self, root, d, frames):
        if d["parent"] == "top":
            frame = LabelFrame(root, text=d["name"])
        else:
            fid = int(d["parent"])
            frame = LabelFrame(frames[fid], text=d["name"])
        frames.append(frame)

        frame.place(**d["geometry"])
        self.d = d
        self.labels  = [ ttk.Label(frame, font="Serif 10", text=label["text"]) for _ , label in d["labels"].items() ]
        for i, label in enumerate([v for k,v in d["labels"].items()]):
            self.labels[i].place(**label["geometry"])
            try:
                self.labels[i].configure(wraplength=label["wraplength"])
            except:
                pass

        self.entries = [ ttk.Entry(frame, font="Times 12 bold", justify='center') for _ , entry in d["entries"].items() ]
        self.sv = [ tk.StringVar() for i in range(0,len(self.entries)) ]
        for i in range(0,len(self.sv)):
            self.entries[i].configure(textvariable=self.sv[i])
            self.sv[i].trace("w", lambda name, index, mode, sv=self.sv[i]: self.handler(sv))

        for i, entry in enumerate([v for k,v in sorted(d["entries"].items())]):
            self.entries[i].place(**entry["geometry"])

        try:
            self.radios  = [ ttk.Radiobutton(frame, text=radio["text"]) for _ , radio in d["radios"].items() ]
            for i in range(5):
                x = tk.IntVar()
                x.set(99)
                self.rv.append(x)

            for i, radio in enumerate([v for k,v in d["radios"].items()]):
                self.radios[i].place(**radio["geometry"])
                self.radios[i].configure(variable=self.rv[radio["variable"]], value=radio["value"])
        except:
            pass

        try:
            self.combos  = [ ttk.Combobox(frame) for _ , combo in d["comboboxes"].items() ]
            for i in range(len(self.combos)):
                x = tk.StringVar()
                x.set("")
                self.cv.append(x)
            for i, combo in enumerate([v for k,v in d["comboboxes"].items()]):
                self.combos[i].place(**combo["geometry"])
                self.combos[i]["values"]=combo["values"]
                self.combos[i].configure(state='readonly', textvariable=self.cv[combo["variable"]])
        except:
            pass


    def get_data(self):
        data = []
        for i, (_, entry) in enumerate(self.d["entries"].items()):
            data.append({\
            "page":entry["realgeometry"]["page"],\
            "string":self.entries[i].get(),\
            "x":entry["realgeometry"]["realx"],\
             "y":entry["realgeometry"]["realy"],\
             "width":entry["realgeometry"]["realwidth"]})

        for i, (_, radio) in enumerate(self.d["radios"].items()):
            string = ""
            if self.rv[radio["variable"]].get() == radio["value"]:
                string = "X"
            data.append({\
            "page":radio["realgeometry"]["page"],\
            "string":string,\
            "x":radio["realgeometry"]["realx"],\
             "y":radio["realgeometry"]["realy"],\
             "width":radio["realgeometry"]["realwidth"]})
        try:
            for _, combo in self.d["comboboxes"].items():
                string = ""
                udata = self.cv[combo["variable"]].get()
                try:
                    index = combo["values"].index(udata)
                    string = "X"
                    data.append({\
                    "page":combo["page"],\
                    "string":string, \
                    "x":combo["realxs"][index],\
                    "y":combo["realys"][index],\
                    "width":combo["realwidths"][index]})
                except:
                    pass
        except:
            pass
        return data
