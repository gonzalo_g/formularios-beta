#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter.ttk import *
from tkinter import ttk
import codecs
import ghostscript
import os
import platform
from subprocess import call

def to_pdf(ftype, name):
    cargs = []
    if platform.system() == "Windows":
      gs = "gswin32c"
      desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
      directory = desktop + "/output"
    else:
      gs = "gs"
      directory = "output"

    if not os.path.exists(directory):
        os.makedirs(directory)


    if ftype == "03":
        res = " -g5730x8590"
    elif ftype == "02":
        res = " -g5730x8650"

    outf = " -sOutputFile=" + directory+"/"+name+".pdf"
    inf = " "+name+".ps"
    print("gs" + " -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite" + res + " -dPDFFitPage " + outf + inf)
    os.system(gs + " -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite" + res + " -dPDFFitPage " + outf + inf)


def centrar (str, x, w):
    diff = w - len(str)*2.1
    return x + diff/2

def printpg(ftype, pag, xp, yp, num):

    h = 0
    hp = 0

    if ftype == "03":
        h = str(303)
        hp = str(859)
    elif ftype == "02":
        h = str(305)
        hp = str(865)

    doc = """%!PS-Adobe-3.0
%%DocumentMedia: 202x{0}mm 573 {1} 0 () ()
%%BoundingBox: 0 0 573 {1}
%%Pages: 1
/inch {{72 mul}} def
/mm {{2.8346 mul}} def
/ox {{0 mm}} def
/oy {{0 mm}} def
/Courier-Bold findfont
dup length dict begin
{{1 index /FID ne {{def}} {{pop pop}} ifelse}} forall
/Encoding ISOLatin1Encoding def
currentdict
end
/TimesIL1 exch definefont pop
/TimesIL1 findfont 10 scalefont setfont
""".format(h, hp)

    flote = 1
    with open('desviaciones/'+ftype+'.txt') as f:
        for line in f:
            xdesv, ydesv = map(float, line.split())

    for p in pag:
        if p["string"]:
            doc += '{} mm {} mm moveto\n'.format(\
                centrar(p["string"], p["x"], p["width"]) + xp + xdesv,\
                p["y"] + yp + flote + ydesv\
                )
            doc += '({}) show\n'.format(p["string"])

    doc += "%%EOF"
    psfile = open("pag"+num+".ps", "w")
    psfile.write(doc)
    psfile.close()
    to_pdf(ftype, "pag"+num)

def printps(ftype, pag1, pag2, xp1, yp1, xp2, yp2):
    printpg(ftype, pag1, xp1, yp1, "1")
    printpg(ftype, pag2, xp2, yp2, "2")
