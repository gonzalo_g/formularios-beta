import re
import json
import sys

'''
import os

directory = os.fsencode(directory_in_str)

for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".py"):
        # print(os.path.join(directory, filename))
        continue
'''

filename = sys.argv[1]
textfile = open(filename, 'r')
filetext = textfile.read()
textfile.close()
type = r"(?P<type>Label|Entry|TCombobox|Labelframe)"
type2 = r"(?P<type>Label|Entry|Combobox)"

x = r"(rel)?x=(?P<x>\d+(\.\d+)?)"
y = r"(rel)?y=(?P<y>\d+(\.\d+)?)"
height = r"(rel)?height=(?P<height>\d+(\.\d+)?)"
width = r"(rel)?width=(?P<width>\d+(\.\d+)?)"

place = r"\.place\({}\, {}\, {}\, {}\)".format(x,y,height,width)

torf = r"(self\.Labelframe)?(?P<parent>top|\d+?)"

bdecl = r".*?self\.Labelframe(?P<id>\d+?)( = )LabelFrame\(" + torf + r"\)(.*?\n)"
bplace = r".*?self\.Labelframe(?P=id)" + place + (r'.*?\n')
bname = r"((.*?)\n(.*?text='''(?P<name>.*?)''')\)\n)?"


widdecl = r".*?self\.(?P<type>Entry|Label|TCombobox)(?P<id>\d+?)( = )(Entry|Label|ttk\.Combobox)\(self\.Labelframe(?P<parent>\d+?)(.*?\n)"
widplace = r".*?self\.(Entry|Label|TCombobox)(?P=id)" + place + (r'.*?\n')
text = r"(((.*?)\n)?(.*?text='''(?P<text>.*?)''')\)\n)?"

bpat = re.compile(bdecl + bplace + bname)
widpat = re.compile(widdecl+widplace+text)

l1 = r"^.*?self\.TCombobox(?P<id>\d+?) = ttk\.Combobox\(self.Labelframe(?P<parent>\d+?)\).*?\n"
l2 = r"^.*?self\.TCombobox(?P=id)\.place\((?P<geom>.*?)\).*?\n"

xpat = re.compile(l1+l2,re.MULTILINE)

matches = xpat.finditer(filetext)
for match in matches:
    print(match.group('geom'))
matches = bpat.finditer(filetext)
blocks = {}
for match in matches:
    blocks[match.group('id')] = {\
    'name': match.group('name'),\
    'parent': match.group('parent'),\
    'geometry':{\
    'x':int(match.group('x')),\
    'y':int(match.group('y')),\
    'height':int(match.group('height')),\
    'width':int(match.group('width'))\
    }, 'labels':[], 'entries':[], 'comboboxes':[], 'radios':[], 'frames':[]}

matches = widpat.finditer(filetext)

for match in matches:
  if (match.group('type') == 'Label'):
      blocks[match.group('parent')]['labels'].append(\
            {\
            'name':"Label"+match.group('id'),
            'text':match.group('text'),\
            'geometry':{\
                        'x':int(float(match.group('x'))*blocks[match.group('parent')]['geometry']['width']),\
                        'y':int(float(match.group('y'))*blocks[match.group('parent')]['geometry']['height']),\
                        'height':int(match.group('height')),\
                        'width':int(match.group('width'))\
                        }\
            }\
            )
  if (match.group('type') == 'Entry'):
      blocks[match.group('parent')]['entries'].append(\
            {\
            'name':"Entry"+match.group('id'),
            'geometry':{\
                        'x':int(match.group('x')),\
                        'y':int(match.group('y')),\
                        'height':int(match.group('height')),\
                        'width':int(match.group('width'))\
                        },\
            'realx':0,\
            'realy':0,\
            'realwidth':0\
            }\
            )
  if (match.group('type') == 'TCombobox'):
      blocks[match.group('parent')]['comboboxes'].append(\
            {\
            'name':'Combo'+match.group('id'),
            'geometry':{\
                        'x':int(match.group('x')),\
                        'y':int(match.group('y')),\
                        'height':int(match.group('height')),\
                        'width':int(match.group('width'))\
                        },\
            'values':[],\
            'realgeom':[]\
            }\
            )

jsonstr = json.dumps(blocks, sort_keys=True, indent=4)
jsonfile = open(sys.argv[2]+".json", "w")
jsonfile.write(jsonstr)
jsonfile.close()
