#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter.ttk import *
from tkinter import ttk
import guitops, form
from datetime import date

limitday = date(2017, 10, 1)
today = date.today()

if today > limitday:
    exit()

root = tk.Tk()
root.geometry("434x168+293+155")
root.resizable(0,0)
root.title("Seleccione el tipo de formulario")
myform = None
rootname = None

def newform(name):
    if name == "03":
        root.title("Solicitud de inscripción contrato prendario (03)")
    else:
        root.title("Certificados, informes y otros (02)")
    
    Button1.destroy()
    Button2.destroy()
    root.geometry('815x600')
    root.geometry("+%d+%d" %  (200, 100))

    global myform
    myform = form.Form(root, name)

    global rootname
    rootname = name

    tkPrintButton = tk.Button(
        root,
        text="Generar",
        command= lambda: calibrate())

    tkPrintButton.place(relx=0.9, rely=0.9, relheight=.1, relwidth=.1)

Button1 = Button(root, command= lambda: newform('02'))
Button1.place(relx=0.16, rely=0.24, height=76, width=107)
Button1.configure(text='''Formulario 02''')

Button2 = Button(root, command= lambda: newform('03'))
Button2.place(relx=0.6, rely=0.24, height=76, width=107)
Button2.configure(text='''Formulario 03''')

def quit():
    global root
    root.destroy()

def printps(top, xp1, yp1, xp2, yp2):
    data = get_data()
    pag1 = [x for x in data if x["page"]==1]
    pag2 = [x for x in data if x["page"]==2]
    top.destroy()
    
    guitops.printps(rootname, pag1, pag2, xp1, yp1, xp2, yp2)

def get_data():
    global myform
    return myform.get_data()

def calibrate():
    top = tk.Toplevel(root)
    top.title("Calibrar")
    top.resizable(0,0)
    top.geometry('400x200')
    top.geometry("+%d+%d" %  (400, 100))

    fp1 = tk.LabelFrame(top, text="Página 1")
    fp1.place(x= 10, y=20, width=185, height=120)

    lxp1 = tk.Label(fp1, text="Margen izquierdo:")
    lxp1.place(x = 10, y=20, height=24)

    exp1 = tk.Entry(fp1, font="Serif 10 bold", justify='center')
    exp1.place(x= 130, y=20, width=40, height=24)
    exp1.insert(0,"7")

    lyp1 = tk.Label(fp1, text="Margen inferior:")
    lyp1.place(x = 10, y=60, height=24)

    eyp1 = tk.Entry(fp1, font="Serif 10 bold", justify='center')
    eyp1.place(x= 130, y=60, width=40, height=24)
    eyp1.insert(0,"5")

    fp2 = tk.LabelFrame(top, text="Página 2")
    fp2.place(x= 205, y=20, width=185, height=120)

    lxp2 = tk.Label(fp2, text="Margen izquierdo:")
    lxp2.place(x = 10, y=20, height=24)

    exp2 = tk.Entry(fp2, font="Serif 10 bold", justify='center')
    exp2.place(x= 130, y=20, width=40, height=24)
    exp2.insert(0,"6")

    lyp2 = tk.Label(fp2, text="Margen inferior:")
    lyp2.place(x = 10, y=60, height=24)

    eyp2 = tk.Entry(fp2, font="Serif 10 bold", justify='center')
    eyp2.place(x= 130, y=60, width=40, height=24)
    eyp2.insert(0,"5")

    button = Button(top, text="ok", command= lambda: printps(top, \
    float(exp1.get()), \
    float(eyp1.get()),\
    float(exp2.get()),\
    float(eyp2.get())\
    ))
    button.place(x=160, y=160)

tk.mainloop()
