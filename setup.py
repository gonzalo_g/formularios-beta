from cx_Freeze import setup, Executable
import os

base = None

shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "iforms",                # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]main.exe",  # Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     None,                     # Icon
     None,                     # IconIndex
     None,                     # ShowCmd
     'TARGETDIR'               # WkDir
     )
    ]

# Now create the table dictionary
msi_data = {"Shortcut": shortcut_table}

# Change some default MSI options and specify the use of the above defined tables
bdist_msi_options = {'data': msi_data}


executables = [Executable("main.py", base="Win32GUI", icon="icon.ico")]

packages = ["ghostscript"]
options = {
    'build_exe': {

        'packages':packages,
        "include_files": ["win32/tcl86t.dll", "win32/tk86t.dll", "layaout/"],
        'include_msvcr': True
    },
    "bdist_msi": bdist_msi_options
}

os.environ['TCL_LIBRARY'] = r'C:\Program Files (x86)\Python36-32\tcl\tcl8.6'
os.environ['TK_LIBRARY'] = r'C:\Program Files (x86)\Python36-32\tcl\tk8.6'

setup(
    name = "iforms",
    options = options,
    version = "1.0",
    description = 'Asistente para el llenado de formularios.',
    executables = executables
)
