#Cargar JSON a diccionario
#Iterar sobre este diccionario creando otro con la nueva estructura
#Convertir a JSON el nuevo diccionario y guardarlo

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json, sys

filepath = sys.argv[1]

jsonfile= open(filepath, "r+")
data = json.load(jsonfile)


jsonfile.seek(0)
jsonstr = json.dumps(data, sort_keys=True, indent=2)
jsonfile.write(jsonstr)
jsonfile.truncate()
jsonfile.close()
